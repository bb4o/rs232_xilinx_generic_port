library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.math_4o.all;
LIBRARY altera_mf;
USE altera_mf.all;



entity tx_buffer is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iNd: in std_logic;
        iData: in std_logic_vector (7 downto 0);

        iBaud_x16: in std_logic;
        oNd: out std_logic;
        oData: out std_logic_vector (7 downto 0);
        oProg_full: out std_logic
    );
end tx_buffer;

architecture v1 of tx_buffer is

    constant cDepth: natural := 512;
    constant cAddr_width: natural := log2(cDepth);
    constant cThreshold: natural := cDepth- cDepth/10;

    COMPONENT scfifo
    GENERIC (
        add_ram_output_register     : STRING;
        intended_device_family      : STRING;
        -- lpm_hint        : STRING;
        lpm_numwords        : NATURAL;
        almost_full_value: natural;
        lpm_showahead       : STRING;
        lpm_type        : STRING;
        lpm_width       : NATURAL;
        lpm_widthu      : NATURAL;
        overflow_checking       : STRING;
        underflow_checking      : STRING;
        use_eab     : STRING
    );
    PORT (
            clock   : IN STD_LOGIC ;
            data    : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
            rdreq   : IN STD_LOGIC ;
            sclr    : IN STD_LOGIC ;
            empty   : OUT STD_LOGIC ;
            q   : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
            wrreq   : IN STD_LOGIC;
            almost_full: out std_logic
    );
    END COMPONENT;

    signal sNd: std_logic;
    signal sEmpty: std_logic;

begin

    oNd <= sNd;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sNd <= '0';
            else
                sNd <= iBaud_x16 and not sEmpty;
            end if;
        end if;
    end process;

    -- empty    <= sub_wire0;
    -- q    <= sub_wire1(6 DOWNTO 0);

    scfifo_component : scfifo
    GENERIC MAP (
        add_ram_output_register => "ON",
        almost_full_value => cThreshold,
        intended_device_family => "Cyclone II",
        -- lpm_hint => "RAM_BLOCK_TYPE=M4K",
        lpm_numwords => cDepth,
        lpm_showahead => "ON",
        lpm_type => "scfifo",
        lpm_width => 8,
        lpm_widthu => cAddr_width,
        overflow_checking => "ON",
        underflow_checking => "ON",
        use_eab => "ON"
    )
    PORT MAP (
        clock => iClk,
        sclr => iReset,
        wrreq => iNd,
        data => iData,
        rdreq => sNd,
        empty => sEmpty,
        q => oData,
        almost_full => oProg_full
    );


end v1;