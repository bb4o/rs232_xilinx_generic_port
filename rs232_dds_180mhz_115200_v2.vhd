library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity rs232_dds_180mhz_115200_v2 is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        oBaud_rate: out std_logic
    );
end rs232_dds_180mhz_115200_v2;

architecture v1 of rs232_dds_180mhz_115200_v2 is

    constant cCap_add: unsigned (11 downto 0) := to_unsigned (3125,12);
    signal sCap: unsigned (11 downto 0);
    signal sCap_dec: unsigned (6 downto 0);
    
    signal sAcc: unsigned (6 downto 0);
    
    signal sCap_reached: std_logic;

--  signal sCnt: unsigned (3 downto 0) := (others => '0');
--  constant cCap: unsigned (3 downto 0) := to_unsigned(10-1,4);

begin

--  process (iCLk)
--  begin
--      if iClk'event and iClk = '1' then
--          if iReset = '1' then
--              sCnt <= (others => '0');
--              oBaud_rate <= '0';
--          else
--              if sCnt = cCap then
--                  sCnt <= (others => '0');
--                  oBaud_rate <= '1';
--              else
--                  sCnt <= sCnt +1;
--                  oBaud_rate <= '0';
--              end if;
--          end if;
--      end if;
--  end process;

    oBaud_rate <= sCap_reached;

    sCap_dec <= sCap(11 downto 5);

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sAcc <= (others => '0');
            else
                sAcc <= sAcc +1;
            end if;
        end if;
    end process;
    
    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sCap_reached <= '0';
            else
                if sAcc = sCap_dec then
                    sCap_reached <= '1';
                else
                    sCap_reached <= '0';
                end if;
            end if;
        end if;
    end process;
    
    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sCap <= (others => '0');
            else
                if sCap_reached = '1' then
                    sCap <= sCap + cCap_add;
                end if;
            end if;
        end if;
    end process;

end v1;