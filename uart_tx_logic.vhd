library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity uart_tx_logic is
  Port (             data_in : in std_logic_vector(7 downto 0);
                en_16_x_baud : in std_logic;
                  serial_out : out std_logic;
                buffer_write : in std_logic;
         buffer_data_present : out std_logic;
            buffer_half_full : out std_logic;
                 buffer_full : out std_logic;
                buffer_reset : in std_logic;
                         clk : in std_logic);
  end uart_tx_logic;


architecture low_level_definition of uart_tx_logic is

component SRL16E_logic is
    generic (
    	INIT: in std_logic_vector (15 downto 0) := X"0000"
    );
    port (
        CLK: in std_logic;
        CE: in std_logic;

    	D: in std_logic;
        A0: in std_logic;
        A1: in std_logic;
        A2: in std_logic;
        A3: in std_logic;
        Q: out std_logic
    );
end component;

component lut6_logic is
   	generic (
      	INIT: std_logic_vector (63 downto 0) := X"0000000000000001"
    ); -- Specify LUT Contents
   	port (
      	O: out std_logic;-- => O,  -- LUT general output
      	I0: in std_logic;-- => I0,   -- LUT input
      	I1: in std_logic;-- => I1,   -- LUT input
      	I2: in std_logic;-- => I2,   -- LUT input
      	I3: in std_logic;-- => I3,   -- LUT input
      	I4: in std_logic;-- => I4,   -- LUT input
      	I5: in std_logic-- => I5    -- LUT input 
   	);
end component;

component lut6_2_logic is
   	generic (
      	INIT: std_logic_vector (63 downto 0) := X"0000000000000001"
    ); -- Specify LUT Contents
   	port (
      	O5: out std_logic;-- => O,  -- LUT general output
      	O6: out std_logic;-- => O,  -- LUT general output
      	I0: in std_logic;-- => I0,   -- LUT input
      	I1: in std_logic;-- => I1,   -- LUT input
      	I2: in std_logic;-- => I2,   -- LUT input
      	I3: in std_logic;-- => I3,   -- LUT input
      	I4: in std_logic;-- => I4,   -- LUT input
      	I5: in std_logic-- => I5    -- LUT input 
   	);
end component;

component fd_logic is
  	port (
	  	D: in std_logic;
	    Q: out std_logic;
	    C: in std_logic
  	);
end component;

component fdr_logic is
  	port (
	  	D: in std_logic;
	    Q: out std_logic;
	    R: in std_logic;
	    C: in std_logic
  	);
end component;
--
-------------------------------------------------------------------------------------------
--
-- Signals used in uart_tx6
--
-------------------------------------------------------------------------------------------
--
signal         store_data : std_logic_vector(7 downto 0);
signal               data : std_logic_vector(7 downto 0);
signal      pointer_value : std_logic_vector(3 downto 0);
signal            pointer : std_logic_vector(3 downto 0);
signal         en_pointer : std_logic;
signal               zero : std_logic;
signal           full_int : std_logic;
signal data_present_value : std_logic;
signal   data_present_int : std_logic;
signal           sm_value : std_logic_vector(3 downto 0);
signal                 sm : std_logic_vector(3 downto 0);
signal          div_value : std_logic_vector(3 downto 0);
signal                div : std_logic_vector(3 downto 0);
signal           lsb_data : std_logic;
signal           msb_data : std_logic;
signal           last_bit : std_logic;
signal        serial_data : std_logic;
signal         next_value : std_logic;
signal           next_bit : std_logic;
signal  buffer_read_value : std_logic;
signal        buffer_read : std_logic;
--
-------------------------------------------------------------------------------------------
--
-- Attributes to guide mapping of logic into Slices.
-------------------------------------------------------------------------------------------
--
--
-- attribute hblknm : string; 
-- attribute hblknm of      pointer3_lut : label is "uart_tx6_1";
-- attribute hblknm of     pointer3_flop : label is "uart_tx6_1";
-- attribute hblknm of      pointer2_lut : label is "uart_tx6_1";
-- attribute hblknm of     pointer2_flop : label is "uart_tx6_1";
-- attribute hblknm of     pointer01_lut : label is "uart_tx6_1";
-- attribute hblknm of     pointer1_flop : label is "uart_tx6_1";
-- attribute hblknm of     pointer0_flop : label is "uart_tx6_1";
-- attribute hblknm of  data_present_lut : label is "uart_tx6_1";
-- attribute hblknm of data_present_flop : label is "uart_tx6_1";
-- --
-- attribute hblknm of           sm0_lut : label is "uart_tx6_2";
-- attribute hblknm of          sm0_flop : label is "uart_tx6_2";
-- attribute hblknm of           sm1_lut : label is "uart_tx6_2";
-- attribute hblknm of          sm1_flop : label is "uart_tx6_2";
-- attribute hblknm of           sm2_lut : label is "uart_tx6_2";
-- attribute hblknm of          sm2_flop : label is "uart_tx6_2";
-- attribute hblknm of           sm3_lut : label is "uart_tx6_2";
-- attribute hblknm of          sm3_flop : label is "uart_tx6_2";
-- --
-- attribute hblknm of         div01_lut : label is "uart_tx6_3";
-- attribute hblknm of         div23_lut : label is "uart_tx6_3";
-- attribute hblknm of         div0_flop : label is "uart_tx6_3";
-- attribute hblknm of         div1_flop : label is "uart_tx6_3";
-- attribute hblknm of         div2_flop : label is "uart_tx6_3";
-- attribute hblknm of         div3_flop : label is "uart_tx6_3";
-- attribute hblknm of          next_lut : label is "uart_tx6_3";
-- attribute hblknm of         next_flop : label is "uart_tx6_3";
-- attribute hblknm of         read_flop : label is "uart_tx6_3";
-- --
-- attribute hblknm of      lsb_data_lut : label is "uart_tx6_4";
-- attribute hblknm of      msb_data_lut : label is "uart_tx6_4";
-- attribute hblknm of        serial_lut : label is "uart_tx6_4";
-- attribute hblknm of       serial_flop : label is "uart_tx6_4";
-- attribute hblknm of          full_lut : label is "uart_tx6_4";
--
--
-------------------------------------------------------------------------------------------
--
-- Start of uart_tx6 circuit description
--
-------------------------------------------------------------------------------------------
--	
begin

  -- SRL16E_logic data storage

  data_width_loop: for i in 0 to 7 generate
    -- attribute hblknm : string; 
    -- attribute hblknm of  storage_srl : label is "uart_tx6_5";
    -- attribute hblknm of storage_flop : label is "uart_tx6_5";

  begin

    storage_srl: SRL16E_logic
    generic map (INIT => X"0000")
    port map(   D => data_in(i),
               CE => buffer_write,
              CLK => clk,
               A0 => pointer(0),
               A1 => pointer(1),
               A2 => pointer(2),
               A3 => pointer(3),
                Q => store_data(i) );

    storage_flop: fd_logic
    port map (  D => store_data(i),
                Q => data(i),
                C => clk);

  end generate data_width_loop;
 

  pointer3_lut: lut6_logic
  generic map (INIT => X"FF00FE00FF80FF00")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => pointer(2),
            I3 => pointer(3),
            I4 => buffer_write,
            I5 => buffer_read,
             O => pointer_value(3));                     

  pointer3_flop: fdr_logic
  port map (  D => pointer_value(3),
              Q => pointer(3),
              R => buffer_reset,
              C => clk);

  pointer2_lut: lut6_logic
  generic map (INIT => X"F0F0E1E0F878F0F0")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => pointer(2),
            I3 => pointer(3),
            I4 => buffer_write,
            I5 => buffer_read,
             O => pointer_value(2));                     

  pointer2_flop: fdr_logic
  port map (  D => pointer_value(2),
              Q => pointer(2),
              R => buffer_reset,
              C => clk);


  pointer01_lut: lut6_2_logic
  generic map (INIT => X"CC9060CCAA5050AA")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => en_pointer,
            I3 => buffer_write,
            I4 => buffer_read,
            I5 => '1',
            O5 => pointer_value(0),
            O6 => pointer_value(1));

  pointer1_flop: fdr_logic
  port map (  D => pointer_value(1),
              Q => pointer(1),
              R => buffer_reset,
              C => clk);

  pointer0_flop: fdr_logic
  port map (  D => pointer_value(0),
              Q => pointer(0),
              R => buffer_reset,
              C => clk);

  data_present_lut: lut6_2_logic
  generic map (INIT => X"F4FCF4FC040004C0")
  port map( I0 => zero,
            I1 => data_present_int,
            I2 => buffer_write,
            I3 => buffer_read,
            I4 => full_int,
            I5 => '1',
            O5 => en_pointer,
            O6 => data_present_value);                     

  data_present_flop: fdr_logic
  port map (  D => data_present_value,
              Q => data_present_int,
              R => buffer_reset,
              C => clk);

  full_lut: lut6_2_logic
  generic map (INIT => X"0001000080000000")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => pointer(2),
            I3 => pointer(3),
            I4 => '1',
            I5 => '1',
            O5 => full_int,
            O6 => zero);

  lsb_data_lut: lut6_logic
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data(0),
            I1 => data(1),
            I2 => data(2),
            I3 => data(3),
            I4 => sm(0),
            I5 => sm(1),
             O => lsb_data);                     


  msb_data_lut: lut6_logic
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data(4),
            I1 => data(5),
            I2 => data(6),
            I3 => data(7),
            I4 => sm(0),
            I5 => sm(1),
             O => msb_data);                     

  serial_lut: lut6_2_logic
  generic map (INIT => X"CFAACC0F0FFFFFFF")
  port map( I0 => lsb_data,
            I1 => msb_data,
            I2 => sm(1),
            I3 => sm(2),
            I4 => sm(3),
            I5 => '1',
            O5 => last_bit,
            O6 => serial_data);

  serial_flop: fd_logic
  port map (  D => serial_data,
              Q => serial_out,
              C => clk);

  sm0_lut: lut6_logic
  generic map (INIT => X"85500000AAAAAAAA")
  port map( I0 => sm(0),
            I1 => sm(1),
            I2 => sm(2),
            I3 => sm(3),
            I4 => data_present_int,
            I5 => next_bit,
             O => sm_value(0));                     

  sm0_flop: fd_logic
  port map (  D => sm_value(0),
              Q => sm(0),
              C => clk);

  sm1_lut: lut6_logic
  generic map (INIT => X"26610000CCCCCCCC")
  port map( I0 => sm(0),
            I1 => sm(1),
            I2 => sm(2),
            I3 => sm(3),
            I4 => data_present_int,
            I5 => next_bit,
             O => sm_value(1));                     

  sm1_flop: fd_logic
  port map (  D => sm_value(1),
              Q => sm(1),
              C => clk);

  sm2_lut: lut6_logic
  generic map (INIT => X"88700000F0F0F0F0")
  port map( I0 => sm(0),
            I1 => sm(1),
            I2 => sm(2),
            I3 => sm(3),
            I4 => data_present_int,
            I5 => next_bit,
             O => sm_value(2));                     

  sm2_flop: fd_logic
  port map (  D => sm_value(2),
              Q => sm(2),
              C => clk);

  sm3_lut: lut6_logic
  generic map (INIT => X"87440000FF00FF00")
  port map( I0 => sm(0),
            I1 => sm(1),
            I2 => sm(2),
            I3 => sm(3),
            I4 => data_present_int,
            I5 => next_bit,
             O => sm_value(3));                     

  sm3_flop: fd_logic
  port map (  D => sm_value(3),
              Q => sm(3),
              C => clk);


  div01_lut: lut6_2_logic
  generic map (INIT => X"6C0000005A000000")
  port map( I0 => div(0),
            I1 => div(1),
            I2 => en_16_x_baud,
            I3 => '1',
            I4 => '1',
            I5 => '1',
            O5 => div_value(0),
            O6 => div_value(1));

  div0_flop: fd_logic
  port map (  D => div_value(0),
              Q => div(0),
              C => clk);

  div1_flop: fd_logic
  port map (  D => div_value(1),
              Q => div(1),
              C => clk);

  div23_lut: lut6_2_logic
  generic map (INIT => X"7F80FF007878F0F0")
  port map( I0 => div(0),
            I1 => div(1),
            I2 => div(2),
            I3 => div(3),
            I4 => en_16_x_baud,
            I5 => '1',
            O5 => div_value(2),
            O6 => div_value(3));

  div2_flop: fd_logic
  port map (  D => div_value(2),
              Q => div(2),
              C => clk);

  div3_flop: fd_logic
  port map (  D => div_value(3),
              Q => div(3),
              C => clk);

  next_lut: lut6_2_logic
  generic map (INIT => X"0000000080000000")
  port map( I0 => div(0),
            I1 => div(1),
            I2 => div(2),
            I3 => div(3),
            I4 => en_16_x_baud,
            I5 => last_bit,
            O5 => next_value,
            O6 => buffer_read_value);

  next_flop: fd_logic
  port map (  D => next_value,
              Q => next_bit,
              C => clk);

  read_flop: fd_logic
  port map (  D => buffer_read_value,
              Q => buffer_read,
              C => clk);


  -- assign internal signals to outputs

  buffer_full <= full_int;  
  buffer_half_full <= pointer(3);  
  buffer_data_present <= data_present_int;

end low_level_definition;

-------------------------------------------------------------------------------------------
--
-- END OF FILE uart_tx6.vhd
--
-------------------------------------------------------------------------------------------


