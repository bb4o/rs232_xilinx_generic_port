library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity uart_logic is
    generic (
        gMake_clk: integer range 0 to 1 := 1;
        gAltera: integer range 0 to 1 := 1
    );
    Port (
        iSYSCLK: in std_logic;
        iReset_ext: in std_logic;

        oClk: out std_logic;
        oReset: out std_logic;

        iRx_pin: in std_logic;
        oTx_pin: out std_logic;
        
        oRx_data: out std_logic_vector(7 downto 0);
        iRx_re: in std_logic;
        oRx_data_present: out std_logic;
        oRx_half_full: out std_logic;
        oRx_full: out std_logic;

        iTx_data: in std_logic_vector(7 downto 0);
        iTx_we: in std_logic;
        oTx_data_present: out std_logic;
        oTx_half_full: out std_logic;
        oTx_full: out std_logic;
        oBaud_x16: out std_logic
    );
end uart_logic;

architecture v1 of uart_logic is

	function int2bool (
		int: integer
	) return boolean is
	begin
		if int = 1 then
			return true;
		elsif int = 0 then
			return false;
		else
			assert false
				report uart_logic'path_name & "invalid generic"
				severity failure;
		end if;
		return false;
	end function;
				
  
	constant cMake_clk: boolean := int2bool (gMake_clk);
   constant cAltera: boolean := int2bool (gAltera);
  
  component uart_rx_logic is
      Port (
          serial_in : in std_logic;
          en_16_x_baud : in std_logic;
          data_out : out std_logic_vector(7 downto 0);
          buffer_read : in std_logic;
          buffer_data_present : out std_logic;
          buffer_half_full : out std_logic;
          buffer_full : out std_logic;
          buffer_reset : in std_logic;
          clk : in std_logic
      );
  end component;

  component uart_tx_logic is
      Port (
          data_in : in std_logic_vector(7 downto 0);
          en_16_x_baud : in std_logic;
          serial_out : out std_logic;
          buffer_write : in std_logic;
          buffer_data_present : out std_logic;
          buffer_half_full : out std_logic;
          buffer_full : out std_logic;
          buffer_reset : in std_logic;
          clk : in std_logic
        );
    end component;

    component rs232_dds_180mhz_115200_v2 is
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            oBaud_rate: out std_logic
        );
    end component;

    signal iBaud_rate: std_logic;
    signal iClk: std_logic;
    signal iReset: std_logic;
    signal sLocked: std_logic;

    component mmcm IS
        PORT (
            inclk0    : IN STD_LOGIC  := '0';
            c0    : OUT STD_LOGIC ;
            locked    : OUT STD_LOGIC 
        );
    END component;

    component mmcm50_to_36 is
        port (
            CLK_IN1           : in     std_logic;
            CLK_OUT1          : out    std_logic;
            LOCKED            : out    std_logic
        );
    end component;

begin

    oBaud_x16 <= iBaud_rate;

    rx: uart_rx_logic
        Port map (
            clk => iClk,
            buffer_reset => iReset,

            en_16_x_baud => iBaud_rate,

            serial_in => iRx_pin,
        
            data_out => oRx_data,
            buffer_read => iRx_re,
            buffer_data_present => oRx_data_present,
            buffer_half_full => oRx_half_full,
            buffer_full => oRx_full
        );

    tx: uart_tx_logic
        Port map (
            clk => iClk,
            buffer_reset => iReset,

            en_16_x_baud => iBaud_rate,

            serial_out => oTx_pin,

            data_in => iTx_data,
            buffer_write => iTx_we,
            buffer_data_present => oTx_data_present,
            buffer_half_full => oTx_half_full,
            buffer_full => oTx_full
        );

    baud_gen: rs232_dds_180mhz_115200_v2
        port map (
            iClk => iClk,
            iReset => iReset,

            oBaud_rate => iBaud_rate
        );

    make_clk: if cMake_clk generate

        altera_mmcm: if cAltera = true generate
        
            clk_gen: mmcm
                PORT map (
                    inclk0 => iSYSCLK,
                    c0 => iClk,
                    locked => sLocked
                );

        end generate;

        xilinx_mmcm: if not cAltera generate

            clk_gen: mmcm50_to_36
                port map (
                    CLK_IN1 => iSYSCLK,
                    CLK_OUT1 => iClk,
                    LOCKED => sLocked
                );

        end generate;

        oClk <= iClk;
        oReset <= iReset;
        iReset <= not sLocked;

    end generate;

    ext_clk: if not cMake_clk generate

        iClk <= iSYSCLK;
        iReset <= iReset_ext;

    end generate;

end v1;