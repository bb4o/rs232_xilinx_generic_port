library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity rs_diff_clk_fifo_wrap is
    port (
        iClk_high: in std_logic;
        iClk_low: in std_logic;
        iReset: in std_logic;

        iRs: in std_logic;
        oRs: out std_logic;

        oEmpty: out std_logic;

        iNd: in std_logic;
        iData: in std_logic_vector (7 downto 0);

        iRd: in std_logic;
        oData: out std_logic_vector (7 downto 0)
    );
end rs_diff_clk_fifo_wrap;

architecture v1 of rs_diff_clk_fifo_wrap is

    component uart_logic_wrap is
        generic (
            gMake_clk: boolean := true;
            -- gLoc: string := "irs=40,ors=41"
            gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            gAltera: boolean := true;
            gActive: boolean := true
        );
        Port (
            iSYSCLK: in std_logic;
            iReset_ext: in std_logic;

            oClk: out std_logic;
            oReset: out std_logic;

            iRs_pin: in std_logic;
            oRs_pin: out std_logic;
            
            iRx_rd: in std_logic;
            oRx_nd: out std_logic;
            oRx_data: out std_logic_vector (7 downto 0);
            oRx_data_present: out std_logic;

            iTx_nd: in std_logic;
            iTx_data: in std_logic_vector (7 downto 0);
            oTx_half_full: out std_logic;
            oBaud_x16: out std_logic
        );
    end component;

    component rs_fifo IS
        PORT (
            aclr        : IN STD_LOGIC  := '0';
            data        : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
            rdclk       : IN STD_LOGIC ;
            rdreq       : IN STD_LOGIC ;
            wrclk       : IN STD_LOGIC ;
            wrreq       : IN STD_LOGIC ;
            q       : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
            rdempty     : OUT STD_LOGIC ;
            wrfull      : OUT STD_LOGIC 
        );
    END component;

    signal sTx_rd: std_logic;
    signal sTx_data: std_logic_vector (7 downto 0);
    signal sTx_empty: std_logic;

    signal sRx_rd: std_logic;
    signal sRx_data: std_logic_vector (7 downto 0);
    signal sRx_data_present: std_logic;

    signal sCe_cnt: unsigned (4 downto 0);
    signal sCe: std_logic;

begin

    process (iClk_low)
    begin
        if iClk_low'event and iClk_low = '1' then
            if iReset = '1' then
                sCe_cnt <= (others => '0');
            else
                sCe_cnt <= '0' & sCe_cnt (sCe_cnt'length-2 downto 0) +1;
            end if;
        end if;
    end process;

    sCe <= sCe_cnt (sCe_cnt'length-1);

    process (iClk_low)
    begin
        if iClk_low'event and iClk_low = '1' then
            if iReset = '1' then
                sTx_rd <= '0';
            else
                if sCe = '1' and sTx_empty = '0' then
                    sTx_rd <= '1';
                else
                    sTx_rd <= '0';
                end if;
            end if;
        end if;
    end process;

    tx_fifo: rs_fifo
        PORT map (
            aclr => iReset,
            data => iData,
            rdclk => iClk_low,
            rdreq => sTx_rd,
            wrclk => iClk_high,
            wrreq => iNd,
            q => sTx_data,
            rdempty => sTx_empty
            -- wrfull => sTx_ful
        );

    rs: uart_logic_wrap
        generic map (
            gMake_clk => false,
            -- gLoc: string := "irs=40,ors=41"
            -- gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            -- gAltera: boolean := true;
            gActive => false
        )
        Port map (
            iSYSCLK => iClk_low,
            iReset_ext => iReset,

            -- oClk: out std_logic;
            -- oReset: out std_logic;

            iRs_pin => iRs,
            oRs_pin => oRs,
            
            iRx_rd => sRx_rd,
            -- oRx_nd: out std_logic;
            oRx_data => sRx_data,
            oRx_data_present => sRx_data_present,

            iTx_nd => sTx_rd,
            iTx_data => sTx_data
            -- oTx_half_full: out std_logic;
            -- oBaud_x16: out std_logic
        );

    process (iClk_low)
    begin
        if iClk_low'event and iClk_low = '1' then
            if iReset = '1' then
                sRx_rd <= '0';
            else
                if sCe = '1' and sRx_data_present = '1' then
                    sRx_rd <= '1';
                else
                    sRx_rd <= '0';
                end if;
            end if;
        end if;
    end process;

    rx_fifo: rs_fifo
        PORT map (
            aclr => iReset,
            data => sRx_data,
            rdclk => iClk_high,
            rdreq => iRd,
            wrclk => iClk_low,
            wrreq => sRx_rd,
            q => oData,
            rdempty => oEmpty
            -- wrfull      : OUT STD_LOGIC 
        );

end v1;