library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity uart_fifo_wrap is
    generic (
        gMake_clk: boolean := true;
        -- gLoc: string := "irs=40,ors=41"
        gLoc: string := "iRs_pin=R7;oRs_pin=T7";
        gAltera: boolean := true
    );
    Port (
        iSYSCLK: in std_logic;
        iReset_ext: in std_logic;

        oClk: out std_logic;
        oReset: out std_logic;

        iRs_pin: in std_logic;
        oRs_pin: out std_logic;
            
        oRx_nd: out std_logic;
        oRx_data: out std_logic_vector (7 downto 0);

        iTx_nd: in std_logic;
        iTx_data: in std_logic_vector (7 downto 0);
        oTx_half_full: out std_logic;
        oBaud_x16: out std_logic
    );
end uart_fifo_wrap;

architecture v1 of uart_fifo_wrap is

    component uart_logic_wrap is
        generic (
            gMake_clk: boolean := true;
            -- gLoc: string := "irs=40,ors=41"
            gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            gAltera: boolean := true
        );
        Port (
            iSYSCLK: in std_logic;
            iReset_ext: in std_logic;

            oClk: out std_logic;
            oReset: out std_logic;

            iRs_pin: in std_logic;
            oRs_pin: out std_logic;
                
            oRx_nd: out std_logic;
            oRx_data: out std_logic_vector (7 downto 0);

            iTx_nd: in std_logic;
            iTx_data: in std_logic_vector (7 downto 0);
            oTx_half_full: out std_logic;
            oBaud_x16: out std_logic
        );
    end component;

    component tx_buffer
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iNd: in std_logic;
            iData: in std_logic_vector (7 downto 0);

            iBaud_x16: in std_logic;
            oNd: out std_logic;
            oData: out std_logic_vector (7 downto 0);
            oProg_full: out std_logic
        );
    end component;

    signal sBaud_x16: std_logic;
    signal sNd: std_logic;
    signal sData: std_logic_vector (7 downto 0);

    signal iClk: std_logic;
    signal iReset: std_logic;

begin

    oBaud_x16 <= sBaud_x16;
    oClk <= iClk;
    oReset <= iReset;

    rs: uart_logic_wrap
        generic map (
            gMake_clk => gMake_clk,
            gLoc => gLoc,
            gAltera => gAltera
        )
        Port map (
            iSYSCLK => iSYSCLK,
            iReset_ext => iReset_ext,

            oClk => iClk,
            oReset => iReset,

            iRs_pin => iRs_pin,
            oRs_pin => oRs_pin,
                
            oRx_nd => oRx_nd,
            oRx_data => oRx_data,

            iTx_nd => sNd,
            iTx_data => sData,
            -- oTx_half_full: out std_logic;
            oBaud_x16 => sBaud_x16
        );

    tx_buffer_wrap: tx_buffer
        port map (
            iClk => iClk,
            iReset => iReset,

            iNd => iTx_nd,
            iData => iTx_data,

            iBaud_x16 => sBaud_x16,
            oNd => sNd,
            oData => sData,
            oProg_full => oTx_half_full
        );

end v1;