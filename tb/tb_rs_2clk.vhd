library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity tb_rs_2clk is
end tb_rs_2clk;

architecture v1 of tb_rs_2clk is

    component uart_high_clk_wrap is
        generic (
            gMake_clk: boolean := true;
            -- gLoc: string := "irs=40,ors=41"
            gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            gAltera: boolean := true;
            gClk_relation: integer := 7
        );
        Port (
            iSYSCLK: in std_logic;
            iClk_high: in std_logic;
            iReset_ext: in std_logic;

            oClk: out std_logic;
            oReset: out std_logic;

            iRs_pin: in std_logic;
            oRs_pin: out std_logic;
                
            oRx_nd: out std_logic;
            oRx_data: out std_logic_vector (7 downto 0);

            iTx_nd: in std_logic;
            iTx_data: in std_logic_vector (7 downto 0);
            oTx_half_full: out std_logic;
            oBaud_x16: out std_logic
        );
    end component;

    -- component uart_logic_wrap is
    --     generic (
    --         gMake_clk: boolean := true;
    --         -- gLoc: string := "irs=40,ors=41"
    --         gLoc: string := "iRs_pin=R7;oRs_pin=T7";
    --         gAltera: boolean := true
    --     );
    --     Port (
    --         iSYSCLK: in std_logic;
    --         iReset_ext: in std_logic;

    --         oClk: out std_logic;
    --         oReset: out std_logic;

    --         iRs_pin: in std_logic;
    --         oRs_pin: out std_logic;
                
    --         oRx_nd: out std_logic;
    --         oRx_data: out std_logic_vector (7 downto 0);

    --         iTx_nd: in std_logic;
    --         iTx_data: in std_logic_vector (7 downto 0);
    --         oTx_half_full: out std_logic;
    --         oBaud_x16: out std_logic
    --     );
    -- end component;

    signal iClk_36: std_logic := '1';
    signal iClk_180: std_logic := '1';
    signal iReset: std_logic := '1';

    signal iRs_pin: std_logic;
    signal oRs_pin: std_logic;                
    signal oRx_nd: std_logic;
    signal oRx_data: std_logic_vector (7 downto 0);
    signal iTx_nd: std_logic;
    signal iTx_data: std_logic_vector (7 downto 0);

    signal sCnt: integer := 0;

begin

    process
    begin
        wait for 14 ns;
        iClk_36 <= not iClk_36;
    end process;

    process
    begin
        wait for 2777 ps;
        iClk_180 <= not iClk_180;
    end process;

    process
    begin
        wait for 56 ns;
        iReset <= '0';
        wait;
    end process;

    iRs_pin <= oRs_pin;

    uut: uart_high_clk_wrap
        generic map (
            gMake_clk => false,
            -- gLoc: string := "irs=40,ors=41"
            -- gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            -- gAltera: boolean := true;
            gClk_relation => 7
        )
        Port map (
            iSYSCLK => iClk_36,
            iClk_high => iClk_180,
            iReset_ext => iReset,

            iRs_pin => iRs_pin,
            oRs_pin => oRs_pin,
            oRx_nd => oRx_nd,
            oRx_data => oRx_data,
            iTx_nd => iTx_nd,
            iTx_data => iTx_data
        );

    process (iClk_180)
    begin
        if iClk_180'event and iClk_180 = '1' then
            if iReset = '1' then
                sCnt <= 0;
            else
                if sCnt < 100 then
                    sCnt <= sCnt +1;
                end if;
            end if;
        end if;
    end process;

    process (iClk_180)
    begin
        if iClk_180'event and iClk_180 = '1' then
            if iReset = '1' then
                iTx_nd <= '0';
                iTx_data <= (others => '0');
            else
                if sCnt = 25 then
                    iTx_nd <= '1';
                    iTx_data <= x"aa";
                else
                    iTx_nd <= '0';
                    iTx_data <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- tester: uart_high_clk_wrap
    --     generic map (
    --         gMake_clk => false,
    --         -- gLoc: string := "irs=40,ors=41"
    --         -- gLoc: string := "iRs_pin=R7;oRs_pin=T7";
    --         -- gAltera: boolean := true;
    --         gClk_relation => 7
    --     )
    --     Port map (
    --         iSYSCLK => iClk_36,
    --         iClk_high => iClk_36,
    --         iReset_ext => iReset,

    --         iRs_pin => iRs_pin,
    --         oRs_pin => oRs_pin,
    --         oRx_nd => oRx_nd,
    --         oRx_data => oRx_data,
    --         iTx_nd => iTx_nd,
    --         iTx_data => iTx_data
    --     );

end v1;