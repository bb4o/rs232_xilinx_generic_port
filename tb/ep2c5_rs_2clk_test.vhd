library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
LIBRARY altera;
USE altera.altera_primitives_components.all;


entity ep2c5_rs_2clk_test is
    port (
        iSYSCLK: in std_logic;

        iRs: in std_logic;
        oRs: out std_logic
    );
end ep2c5_rs_2clk_test;

architecture v1 of ep2c5_rs_2clk_test is
    
    attribute chip_pin: string;
    attribute chip_pin of iSYSCLK: signal is "17";
    attribute chip_pin of iRs: signal is "40";
    attribute chip_pin of oRs: signal is "41";
    
    component mmcm50_200 IS
        PORT (
            inclk0      : IN STD_LOGIC  := '0';
            c0      : OUT STD_LOGIC ;
            c1      : OUT STD_LOGIC ;
            locked      : OUT STD_LOGIC 
        );
    END component;

    component p4o_wrap is
        port (
            iClk_high: in std_logic;
            iClk_low: in std_logic;
            iReset: in std_logic;

            iRs: in std_logic;
            oRs: out std_logic

            -- oTwo: out std_logic_vector (7 downto 0)
        );
    end component;

    signal iClk_180: std_logic;
    signal iClk_36: std_logic;
    signal sLocked: std_logic;
    signal iReset: std_logic;

    component uart_logic_wrap is
        generic (
            gMake_clk: boolean := true;
            -- gLoc: string := "irs=40,ors=41"
            gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            gAltera: boolean := true;
            gActive: boolean := true
        );
        Port (
            iSYSCLK: in std_logic;
            iReset_ext: in std_logic;

            oClk: out std_logic;
            oReset: out std_logic;

            iRs_pin: in std_logic;
            oRs_pin: out std_logic;
            
            iRx_rd: in std_logic;
            oRx_nd: out std_logic;
            oRx_data: out std_logic_vector (7 downto 0);
            oRx_data_present: out std_logic;

            iTx_nd: in std_logic;
            iTx_data: in std_logic_vector (7 downto 0);
            oTx_half_full: out std_logic;
            oBaud_x16: out std_logic
        );
    end component;

    signal sDebug_nd: std_logic;
    signal sDebug_data: std_logic_vector (7 downto 0);

begin

    clk_200: mmcm50_200
        PORT map (
            inclk0 => iSYSCLK,
            c0 => iClk_36,
            c1 => iClk_180,
            locked => sLocked
        );

    iReset <= not sLocked;

    p4o: p4o_wrap
        port map (
            iClk_high => iClk_180,
            iClk_low => iClk_36,
            iReset => iReset,

            iRs => iRs,
            oRs => oRs

            -- oTwo: out std_logic_vector (7 downto 0)
        );

end v1;