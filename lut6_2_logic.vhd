library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity lut6_2_logic is
   	generic (
      	INIT: std_logic_vector (63 downto 0) := X"0000000000000001"
    ); -- Specify LUT Contents
   	port (
      	O5: out std_logic;-- => O,  -- LUT general output
      	O6: out std_logic;-- => O,  -- LUT general output
      	I0: in std_logic;-- => I0,   -- LUT input
      	I1: in std_logic;-- => I1,   -- LUT input
      	I2: in std_logic;-- => I2,   -- LUT input
      	I3: in std_logic;-- => I3,   -- LUT input
      	I4: in std_logic;-- => I4,   -- LUT input
      	I5: in std_logic-- => I5    -- LUT input 
   	);
end lut6_2_logic;

architecture v1 of lut6_2_logic is

	signal sCase6: std_logic_vector (5 downto 0);
	-- signal sCase5: std_logic_vector (4 downto 0);
	
begin

	sCase6 <= I5 & I4 & I3 & I2 & I1 & I0;
	-- sCase5 <= I4 & I3 & I2 & I1 & I0;

	process (sCase6)
	begin
		O5 <= INIT(to_integer(unsigned(sCase6(4 downto 0))));
		O6 <= INIT(to_integer(unsigned(sCase6)));
	end process;

end v1;