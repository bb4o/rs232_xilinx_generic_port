library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity uart_high_clk_wrap is
    generic (
        -- gLoc: string := "irs=40,ors=41"
        gLoc: string := "iRs_pin=R7;oRs_pin=T7";
        gAltera: boolean := true;
        gClk_relation: integer := 7;
        gActive: boolean := true
    );
    Port (
        iSYSCLK: in std_logic;
        iClk_high: in std_logic;
        iReset_ext: in std_logic;

        oClk: out std_logic;
        oReset: out std_logic;

        iRs_pin: in std_logic;
        oRs_pin: out std_logic;

        iRx_rd: in std_logic;
        oRx_nd: out std_logic;
        oRx_data: out std_logic_vector (7 downto 0);
        oRx_data_present: out std_logic;

        iTx_nd: in std_logic;
        iTx_data: in std_logic_vector (7 downto 0);
        oTx_half_full: out std_logic;
        oBaud_x16: out std_logic
    );
end uart_high_clk_wrap;

architecture v1 of uart_high_clk_wrap is

    component uart_logic_wrap is
        generic (
            gMake_clk: boolean := true;
            -- gLoc: string := "irs=40,ors=41"
            gLoc: string := "iRs_pin=R7;oRs_pin=T7";
            gAltera: boolean := true;
            gActive: boolean := true
        );
        Port (
            iSYSCLK: in std_logic;
            iReset_ext: in std_logic;

            oClk: out std_logic;
            oReset: out std_logic;

            iRs_pin: in std_logic;
            oRs_pin: out std_logic;

            iRx_rd: in std_logic;
            oRx_nd: out std_logic;
            oRx_data: out std_logic_vector (7 downto 0);
            oRx_data_present: out std_logic;

            iTx_nd: in std_logic;
            iTx_data: in std_logic_vector (7 downto 0);
            oTx_half_full: out std_logic;
            oBaud_x16: out std_logic
        );
    end component;

    signal sRd_delay: std_logic_vector (gClk_relation-1 downto 0);

    signal sRd: std_logic;

    signal sWr_delay: std_logic_vector (gClk_relation-1 downto 0);
    signal sTx_nd: std_logic;

begin

    process (iClk_high)
    begin
        if iClk_high'event and iClk_high = '1' then
            if iReset_ext = '1' then
                sRd_delay <= (others => '0');
            else
                sRd_delay <= sRd_delay (gClk_relation-2 downto 0) & iRx_rd;
            end if;
        end if;
    end process;

    process (iClk_high)
    begin
        if iClk_high'event and iClk_high = '1' then
            if iReset_ext = '1' then
                sRd <= '0';
            else
                if sRd_delay = (sRd_delay'range => '0') then
                    sRd <= '0';
                else
                    sRd <= '1';
                end if;
            end if;
        end if;
    end process;

    rs: uart_logic_wrap
        generic map (
            gMake_clk => false,
            gLoc => gLoc,
            gAltera => gAltera,
            gActive => gActive
        )
        Port map (
            iSYSCLK => iSYSCLK,
            iReset_ext => iReset_ext,

            -- oClk => sClk_int,
            -- oReset => sReset_int,

            iRs_pin => iRs_pin,
            oRs_pin => oRs_pin,

            iRx_rd => sRd,
            -- oRx_nd => sRx_nd,
            oRx_data => oRx_data,
            oRx_data_present => oRx_data_present,

            iTx_nd => sTx_nd,
            iTx_data => iTx_data,
            oTx_half_full => oTx_half_full,
            oBaud_x16 => oBaud_x16
        );

    process (iClk_high)
    begin
        if iClk_high'event and iClk_high = '1' then
            if iReset_ext = '1' then
                sWr_delay <= (others => '0');
            else
                sWr_delay <= sWr_delay (gClk_relation-2 downto 0) & iTx_nd;
            end if;
        end if;
    end process;

    process (iClk_high)
    begin
        if iClk_high'event and iClk_high = '1' then
            if iReset_ext = '1' then
                sTx_nd <= '0';
            else
                if sWr_delay = (sWr_delay'range => '0') then
                    sTx_nd <= '0';
                else
                    sTx_nd <= '1';
                end if;
            end if;
        end if;
    end process;

end v1;