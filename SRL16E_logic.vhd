library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity SRL16E_logic is
    generic (
    	INIT: in std_logic_vector (15 downto 0) := X"0000"
    );
    port (
        CLK: in std_logic;
        CE: in std_logic;

    	D: in std_logic;
        A0: in std_logic;
        A1: in std_logic;
        A2: in std_logic;
        A3: in std_logic;
        Q: out std_logic
    );
end SRL16E_logic;

architecture v1 of SRL16E_logic is

	signal sReg: std_logic_vector (15 downto 0) := INIT;

	signal sSelect: std_logic_vector (3 downto 0);

begin


    -- storage_srl: SRL16E
    -- generic map (INIT => X"0000")
    -- port map(   D => D,
    --            CE => CE,
    --           CLK => CLK,
    --            A0 => A0,
    --            A1 => A1,
    --            A2 => A2,
    --            A3 => A3,
    --             Q => Q);

	process (CLK)
	begin
		if CLK'event and CLK = '1' then
			if CE = '1' then
				sReg <= sReg (14 downto 0) & D;
			end if;
		end if;
	end process;

	sSelect <= A3 & A2 & A1 & A0;

	Q <= sReg (to_integer(unsigned(sSelect)));

end v1;