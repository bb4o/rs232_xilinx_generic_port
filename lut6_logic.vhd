library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity lut6_logic is
   	generic (
      	INIT: std_logic_vector (63 downto 0) := X"0000000000000001"
    ); -- Specify LUT Contents
   	port (
      	O: out std_logic;-- => O,  -- LUT general output
      	I0: in std_logic;-- => I0,   -- LUT input
      	I1: in std_logic;-- => I1,   -- LUT input
      	I2: in std_logic;-- => I2,   -- LUT input
      	I3: in std_logic;-- => I3,   -- LUT input
      	I4: in std_logic;-- => I4,   -- LUT input
      	I5: in std_logic-- => I5    -- LUT input 
   	);
end lut6_logic;

architecture v1 of lut6_logic is

	signal sCase: std_logic_vector (5 downto 0);
	
begin

	sCase <= I5 & I4 & I3 & I2 & I1 & I0;

	-- process (sCase)
	-- begin
	O <= INIT(to_integer(unsigned(sCase)));

		-- case sCase is
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>
		-- 	when "000000" =>

end v1;