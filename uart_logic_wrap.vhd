library ieee;
use ieee.std_logic_1164.all;

entity uart_logic_wrap is
	generic (
		gMake_clk: boolean := true;
		-- gLoc: string := "irs=40,ors=41"
		gLoc: string := "iRs_pin=R7;oRs_pin=T7";
		gAltera: boolean := true;
		gActive: boolean := true
	);
  	Port (
        iSYSCLK: in std_logic;
        iReset_ext: in std_logic;

        oClk: out std_logic;
        oReset: out std_logic;

		iRs_pin: in std_logic;
        oRs_pin: out std_logic;
	  	
	  	iRx_rd: in std_logic;
        oRx_nd: out std_logic;
        oRx_data: out std_logic_vector (7 downto 0);
        oRx_data_present: out std_logic;
        oTx_full: out std_logic;

		iTx_nd: in std_logic;
		iTx_data: in std_logic_vector (7 downto 0);
        oTx_half_full: out std_logic;
        oBaud_x16: out std_logic
    );
end uart_logic_wrap;

architecture v1 of uart_logic_wrap is

	function mk_iface_config (
		loc_str: string;
		pin_name: string
	) return string is
		constant loc_str_length: integer := loc_str'length;
		variable name_found: boolean := false;
		variable pos_start: integer;
		variable pos_end: integer;
		constant cSearch_loop_max: integer := loc_str_length - pin_name'length;
	begin
		--define values;
			name_found := false;
			search_loop: for j in 1 to cSearch_loop_max loop
				report "j = " & integer'image(j);
				report "checking " & loc_str (j to j+pin_name'length);
				if (loc_str (j to j+pin_name'length-1) = pin_name) then
					name_found := true;
					pos_start := j+pin_name'length+1;
					exit search_loop;
				end if;
			end loop;
			if not name_found then
				assert false
					report uart_logic_wrap'path_name & "failed to find loc for port "
						& pin_name
					severity failure;
			end if;

			assert loc_str(pos_start-1) = '='
				report uart_logic_wrap'path_name &
					"no '=' after name " & pin_name
					severity failure;

			pos_end := pos_start;
			name_search: for i in pos_end to loc_str_length loop
				if loc_str(i) = ';' then
					report "found end at " & integer'image(i);
					pos_end := i-1;
					exit name_search;
				else
					pos_end := i;
				end if;
			end loop;
			report "returning " & loc_str (pos_start to pos_end);
			return loc_str (pos_start to pos_end);
			--loop to 
	end function;

	-- attribute loc: string;
	-- attribute loc of iRs_pin: signal is mk_iface_config(gLoc, "iRs_pin");
	-- attribute loc of oRs_pin: signal is mk_iface_config(gLoc, "oRs_pin");

	-- attribute chip_pin: string;
	-- attribute chip_pin of iRs_pin: signal is mk_iface_config(gLoc, "iRs_pin");
	-- attribute chip_pin of oRs_pin: signal is mk_iface_config(gLoc, "oRs_pin");

	component uart_logic is
		generic (
			gMake_clk: boolean := true;
			gAltera: boolean := true
		);
	  	Port (
	        iSYSCLK: in std_logic;
	        iReset_ext: in std_logic;

	        oClk: out std_logic;
	        oReset: out std_logic;

			iRx_pin: in std_logic;
	        oTx_pin: out std_logic;
		  		
	        oRx_data: out std_logic_vector(7 downto 0);
	        iRx_re: in std_logic;
	        oRx_data_present: out std_logic;
	        oRx_half_full: out std_logic;
	        oRx_full: out std_logic;

			iTx_data: in std_logic_vector(7 downto 0);
	        iTx_we: in std_logic;
	     	oTx_data_present: out std_logic;
	        oTx_half_full: out std_logic;
	        oTx_full: out std_logic;
        	oBaud_x16: out std_logic
	    );
	end component;

	signal sRe: std_logic;
	signal sRd: std_logic;

begin

	oRx_data_present <= sRe;

	active: if gActive generate
		oRx_nd <= sRe;
		sRd <= sRe;
	end generate;

	passive: if not gActive generate
		oRx_nd <= iRx_rd;
		sRd <= iRx_rd;
	end generate;

	rs: uart_logic
		generic map (
			gMake_clk => gMake_clk,
			gAltera => gAltera
		)
	  	Port map (
	        iSYSCLK => iSYSCLK,
	        iReset_ext => iReset_ext,

	        oClk => oClk,
	        oReset => oReset,

			iRx_pin => iRs_pin,
	        oTx_pin => oRs_pin,
		  		
	        oRx_data => oRx_data,
	        iRx_re => sRd,
	        oRx_data_present => sRe,
	        -- oRx_half_full: out std_logic;
	        -- oRx_full: out std_logic;

			iTx_data => iTx_data,
	        iTx_we => iTx_nd,
	     	-- oTx_data_present: out std_logic;
	        oTx_half_full => oTx_half_full,
	        oTx_full => oTx_full,
        	oBaud_x16 => oBaud_x16
	    );

end v1;