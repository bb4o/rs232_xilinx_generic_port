library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fd_logic is
  	port (
	  	D: in std_logic;
	    Q: out std_logic;
	    C: in std_logic
  	);
end fd_logic;

architecture v1 of fd_logic is

begin

	process (C)
	begin
		if C'event and C = '1' then
			Q <= D;
		end if;
	end process;

end v1;