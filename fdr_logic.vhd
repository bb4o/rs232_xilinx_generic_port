library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fdr_logic is
  	port (
	  	D: in std_logic;
	    Q: out std_logic;
	    R: in std_logic;
	    C: in std_logic
  	);
end fdr_logic;

architecture v1 of fdr_logic is

begin

	process (C)
	begin
		if C'event and C = '1' then
			if R = '1' then
				Q <= '0';
			else
				Q <= D;
			end if;
		end if;
	end process;

end v1;