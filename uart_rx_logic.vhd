library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity uart_rx_logic is
  	Port (
  		serial_in : in std_logic;
        en_16_x_baud : in std_logic;
        data_out : out std_logic_vector(7 downto 0);
        buffer_read : in std_logic;
        buffer_data_present : out std_logic;
        buffer_half_full : out std_logic;
        buffer_full : out std_logic;
        buffer_reset : in std_logic;
        clk : in std_logic
    );
end uart_rx_logic;

architecture v1 of uart_rx_logic is

component SRL16E_logic is
    generic (
    	INIT: in std_logic_vector (15 downto 0) := X"0000"
    );
    port (
        CLK: in std_logic;
        CE: in std_logic;

    	D: in std_logic;
        A0: in std_logic;
        A1: in std_logic;
        A2: in std_logic;
        A3: in std_logic;
        Q: out std_logic
    );
end component;

component lut6_logic is
   	generic (
      	INIT: std_logic_vector (63 downto 0) := X"0000000000000001"
    ); -- Specify LUT Contents
   	port (
      	O: out std_logic;-- => O,  -- LUT general output
      	I0: in std_logic;-- => I0,   -- LUT input
      	I1: in std_logic;-- => I1,   -- LUT input
      	I2: in std_logic;-- => I2,   -- LUT input
      	I3: in std_logic;-- => I3,   -- LUT input
      	I4: in std_logic;-- => I4,   -- LUT input
      	I5: in std_logic-- => I5    -- LUT input 
   	);
end component;

component lut6_2_logic is
   	generic (
      	INIT: std_logic_vector (63 downto 0) := X"0000000000000001"
    ); -- Specify LUT Contents
   	port (
      	O5: out std_logic;-- => O,  -- LUT general output
      	O6: out std_logic;-- => O,  -- LUT general output
      	I0: in std_logic;-- => I0,   -- LUT input
      	I1: in std_logic;-- => I1,   -- LUT input
      	I2: in std_logic;-- => I2,   -- LUT input
      	I3: in std_logic;-- => I3,   -- LUT input
      	I4: in std_logic;-- => I4,   -- LUT input
      	I5: in std_logic-- => I5    -- LUT input 
   	);
end component;

component fd_logic is
  	port (
	  	D: in std_logic;
	    Q: out std_logic;
	    C: in std_logic
  	);
end component;

component fdr_logic is
  	port (
	  	D: in std_logic;
	    Q: out std_logic;
	    R: in std_logic;
	    C: in std_logic
  	);
end component;
--
-------------------------------------------------------------------------------------------
--
-- Signals used in uart_rx6
--
-------------------------------------------------------------------------------------------
--
signal      pointer_value : std_logic_vector(3 downto 0);
signal            pointer : std_logic_vector(3 downto 0);
signal         en_pointer : std_logic;
signal               zero : std_logic;
signal           full_int : std_logic;
signal data_present_value : std_logic;
signal   data_present_int : std_logic;
signal       sample_value : std_logic;
signal             sample : std_logic;
signal   sample_dly_value : std_logic;
signal         sample_dly : std_logic;
signal     stop_bit_value : std_logic;
signal           stop_bit : std_logic;
signal         data_value : std_logic_vector(7 downto 0);
signal               data : std_logic_vector(7 downto 0);
signal          run_value : std_logic;
signal                run : std_logic;
signal    start_bit_value : std_logic;
signal          start_bit : std_logic;
signal          div_value : std_logic_vector(3 downto 0);
signal                div : std_logic_vector(3 downto 0);
signal          div_carry : std_logic;
signal sample_input_value : std_logic;
signal       sample_input : std_logic;
signal buffer_write_value : std_logic;
signal       buffer_write : std_logic;
--
-------------------------------------------------------------------------------------------
--
-- Attributes to guide mapping of logic into Slices.
-------------------------------------------------------------------------------------------
--
--
-- attribute hblknm : string; 
-- attribute hblknm of      pointer3_lut : label is "uart_rx6_1";
-- attribute hblknm of     pointer3_flop : label is "uart_rx6_1";
-- attribute hblknm of      pointer2_lut : label is "uart_rx6_1";
-- attribute hblknm of     pointer2_flop : label is "uart_rx6_1";
-- attribute hblknm of     pointer01_lut : label is "uart_rx6_1";
-- attribute hblknm of     pointer1_flop : label is "uart_rx6_1";
-- attribute hblknm of     pointer0_flop : label is "uart_rx6_1";
-- attribute hblknm of  data_present_lut : label is "uart_rx6_1";
-- attribute hblknm of data_present_flop : label is "uart_rx6_1";
-- --
-- attribute hblknm of        data01_lut : label is "uart_rx6_2";
-- attribute hblknm of        data0_flop : label is "uart_rx6_2";
-- attribute hblknm of        data1_flop : label is "uart_rx6_2";
-- attribute hblknm of        data23_lut : label is "uart_rx6_2";
-- attribute hblknm of        data2_flop : label is "uart_rx6_2";
-- attribute hblknm of        data3_flop : label is "uart_rx6_2";
-- attribute hblknm of        data45_lut : label is "uart_rx6_2";
-- attribute hblknm of        data4_flop : label is "uart_rx6_2";
-- attribute hblknm of        data5_flop : label is "uart_rx6_2";
-- attribute hblknm of        data67_lut : label is "uart_rx6_2";
-- attribute hblknm of        data6_flop : label is "uart_rx6_2";
-- attribute hblknm of        data7_flop : label is "uart_rx6_2";
-- --
-- attribute hblknm of         div01_lut : label is "uart_rx6_3";
-- attribute hblknm of         div23_lut : label is "uart_rx6_3";
-- attribute hblknm of         div0_flop : label is "uart_rx6_3";
-- attribute hblknm of         div1_flop : label is "uart_rx6_3";
-- attribute hblknm of         div2_flop : label is "uart_rx6_3";
-- attribute hblknm of         div3_flop : label is "uart_rx6_3";
-- attribute hblknm of  sample_input_lut : label is "uart_rx6_3";
-- attribute hblknm of sample_input_flop : label is "uart_rx6_3";
-- attribute hblknm of          full_lut : label is "uart_rx6_3";
-- --
-- attribute hblknm of        sample_lut : label is "uart_rx6_4";
-- attribute hblknm of       sample_flop : label is "uart_rx6_4";
-- attribute hblknm of   sample_dly_flop : label is "uart_rx6_4";
-- attribute hblknm of      stop_bit_lut : label is "uart_rx6_4";
-- attribute hblknm of     stop_bit_flop : label is "uart_rx6_4";
-- attribute hblknm of buffer_write_flop : label is "uart_rx6_4";
-- attribute hblknm of     start_bit_lut : label is "uart_rx6_4";
-- attribute hblknm of    start_bit_flop : label is "uart_rx6_4";
-- attribute hblknm of           run_lut : label is "uart_rx6_4";
-- attribute hblknm of          run_flop : label is "uart_rx6_4";
--
--
-------------------------------------------------------------------------------------------
--
-- Start of uart_rx6 circuit description
--
-------------------------------------------------------------------------------------------
--	
begin

  -- SRL16E_logic data storage

  data_width_loop: for i in 0 to 7 generate
    -- attribute hblknm : string; 
    -- attribute hblknm of  storage_srl : label is "uart_rx6_5";

  begin

    storage_srl: SRL16E_logic
    generic map (INIT => X"0000")
    port map(   D => data(i),
               CE => buffer_write,
              CLK => clk,
               A0 => pointer(0),
               A1 => pointer(1),
               A2 => pointer(2),
               A3 => pointer(3),
                Q => data_out(i) );

  end generate data_width_loop;
 

  pointer3_lut: lut6_logic
  generic map (INIT => X"FF00FE00FF80FF00")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => pointer(2),
            I3 => pointer(3),
            I4 => buffer_write,
            I5 => buffer_read,
             O => pointer_value(3));                     

  pointer3_flop: fdr_logic
  port map (  D => pointer_value(3),
              Q => pointer(3),
              R => buffer_reset,
              C => clk);

  pointer2_lut: lut6_logic
  generic map (INIT => X"F0F0E1E0F878F0F0")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => pointer(2),
            I3 => pointer(3),
            I4 => buffer_write,
            I5 => buffer_read,
             O => pointer_value(2));                     

  pointer2_flop: fdr_logic
  port map (  D => pointer_value(2),
              Q => pointer(2),
              R => buffer_reset,
              C => clk);


  pointer01_lut: lut6_2_logic
  generic map (INIT => X"CC9060CCAA5050AA")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => en_pointer,
            I3 => buffer_write,
            I4 => buffer_read,
            I5 => '1',
            O5 => pointer_value(0),
            O6 => pointer_value(1));

  pointer1_flop: fdr_logic
  port map (  D => pointer_value(1),
              Q => pointer(1),
              R => buffer_reset,
              C => clk);

  pointer0_flop: fdr_logic
  port map (  D => pointer_value(0),
              Q => pointer(0),
              R => buffer_reset,
              C => clk);

  data_present_lut: lut6_2_logic
  generic map (INIT => X"F4FCF4FC040004C0")
  port map( I0 => zero,
            I1 => data_present_int,
            I2 => buffer_write,
            I3 => buffer_read,
            I4 => full_int,
            I5 => '1',
            O5 => en_pointer,
            O6 => data_present_value);                     

  data_present_flop: fdr_logic
  port map (  D => data_present_value,
              Q => data_present_int,
              R => buffer_reset,
              C => clk);

  full_lut: lut6_2_logic
  generic map (INIT => X"0001000080000000")
  port map( I0 => pointer(0),
            I1 => pointer(1),
            I2 => pointer(2),
            I3 => pointer(3),
            I4 => '1',
            I5 => '1',
            O5 => full_int,
            O6 => zero);

  sample_lut: lut6_2_logic
  generic map (INIT => X"CCF00000AACC0000")
  port map( I0 => serial_in,
            I1 => sample,
            I2 => sample_dly,
            I3 => en_16_x_baud,
            I4 => '1',
            I5 => '1',
            O5 => sample_value,
            O6 => sample_dly_value);

  sample_flop: fd_logic
  port map (  D => sample_value,
              Q => sample,
              C => clk);

  sample_dly_flop: fd_logic
  port map (  D => sample_dly_value,
              Q => sample_dly,
              C => clk);

  stop_bit_lut: lut6_2_logic
  generic map (INIT => X"CAFFCAFF0000C0C0")
  port map( I0 => stop_bit,
            I1 => sample,
            I2 => sample_input,
            I3 => run,
            I4 => data(0),
            I5 => '1',
            O5 => buffer_write_value,
            O6 => stop_bit_value);

  buffer_write_flop: fd_logic
  port map (  D => buffer_write_value,
              Q => buffer_write,
              C => clk);

  stop_bit_flop: fd_logic
  port map (  D => stop_bit_value,
              Q => stop_bit,
              C => clk);

  data01_lut: lut6_2_logic
  generic map (INIT => X"F0CCFFFFCCAAFFFF")
  port map( I0 => data(0),
            I1 => data(1),
            I2 => data(2),
            I3 => sample_input,
            I4 => run,
            I5 => '1',
            O5 => data_value(0),
            O6 => data_value(1));

  data0_flop: fd_logic
  port map (  D => data_value(0),
              Q => data(0),
              C => clk);

  data1_flop: fd_logic
  port map (  D => data_value(1),
              Q => data(1),
              C => clk);


  data23_lut: lut6_2_logic
  generic map (INIT => X"F0CCFFFFCCAAFFFF")
  port map( I0 => data(2),
            I1 => data(3),
            I2 => data(4),
            I3 => sample_input,
            I4 => run,
            I5 => '1',
            O5 => data_value(2),
            O6 => data_value(3));

  data2_flop: fd_logic
  port map (  D => data_value(2),
              Q => data(2),
              C => clk);

  data3_flop: fd_logic
  port map (  D => data_value(3),
              Q => data(3),
              C => clk);

  data45_lut: lut6_2_logic
  generic map (INIT => X"F0CCFFFFCCAAFFFF")
  port map( I0 => data(4),
            I1 => data(5),
            I2 => data(6),
            I3 => sample_input,
            I4 => run,
            I5 => '1',
            O5 => data_value(4),
            O6 => data_value(5));

  data4_flop: fd_logic
  port map (  D => data_value(4),
              Q => data(4),
              C => clk);

  data5_flop: fd_logic
  port map (  D => data_value(5),
              Q => data(5),
              C => clk);

  data67_lut: lut6_2_logic
  generic map (INIT => X"F0CCFFFFCCAAFFFF")
  port map( I0 => data(6),
            I1 => data(7),
            I2 => stop_bit,
            I3 => sample_input,
            I4 => run,
            I5 => '1',
            O5 => data_value(6),
            O6 => data_value(7));

  data6_flop: fd_logic
  port map (  D => data_value(6),
              Q => data(6),
              C => clk);

  data7_flop: fd_logic
  port map (  D => data_value(7),
              Q => data(7),
              C => clk);

  run_lut: lut6_logic
  generic map (INIT => X"2F2FAFAF0000FF00")
  port map( I0 => data(0),
            I1 => start_bit,
            I2 => sample_input,
            I3 => sample_dly,
            I4 => sample,
            I5 => run,
             O => run_value);                     

  run_flop: fd_logic
  port map (  D => run_value,
              Q => run,
              C => clk);

  start_bit_lut: lut6_logic
  generic map (INIT => X"222200F000000000")
  port map( I0 => start_bit,
            I1 => sample_input,
            I2 => sample_dly,
            I3 => sample,
            I4 => run,
            I5 => '1',
             O => start_bit_value);                     

  start_bit_flop: fd_logic
  port map (  D => start_bit_value,
              Q => start_bit,
              C => clk);

  div01_lut: lut6_2_logic
  generic map (INIT => X"6C0000005A000000")
  port map( I0 => div(0),
            I1 => div(1),
            I2 => en_16_x_baud,
            I3 => run,
            I4 => '1',
            I5 => '1',
            O5 => div_value(0),
            O6 => div_value(1));

  div0_flop: fd_logic
  port map (  D => div_value(0),
              Q => div(0),
              C => clk);

  div1_flop: fd_logic
  port map (  D => div_value(1),
              Q => div(1),
              C => clk);

  div23_lut: lut6_2_logic
  generic map (INIT => X"6CCC00005AAA0000")
  port map( I0 => div(2),
            I1 => div(3),
            I2 => div_carry,
            I3 => en_16_x_baud,
            I4 => run,
            I5 => '1',
            O5 => div_value(2),
            O6 => div_value(3));

  div2_flop: fd_logic
  port map (  D => div_value(2),
              Q => div(2),
              C => clk);

  div3_flop: fd_logic
  port map (  D => div_value(3),
              Q => div(3),
              C => clk);

  sample_input_lut: lut6_2_logic
  generic map (INIT => X"0080000088888888")
  port map( I0 => div(0),
            I1 => div(1),
            I2 => div(2),
            I3 => div(3),
            I4 => en_16_x_baud,
            I5 => '1',
            O5 => div_carry,
            O6 => sample_input_value);

  sample_input_flop: fd_logic
  port map (  D => sample_input_value,
              Q => sample_input,
              C => clk);


  -- assign internal signals to outputs

  buffer_full <= full_int;  
  buffer_half_full <= pointer(3);  
  buffer_data_present <= data_present_int;

end v1;